package ru.t1.dkandakov.tm.repository;

import ru.t1.dkandakov.tm.api.repository.IProjectRepository;
import ru.t1.dkandakov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projects.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public int getSize() {
        return projects.size();
    }

    @Override
    public boolean existsById(String projectId) {
        if (findOneById(projectId) == null) return false;
        else return true;
    }

}