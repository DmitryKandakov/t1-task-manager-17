package ru.t1.dkandakov.tm.command.system;

import ru.t1.dkandakov.tm.api.service.ICommandService;
import ru.t1.dkandakov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}